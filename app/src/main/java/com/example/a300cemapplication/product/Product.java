package com.example.a300cemapplication.product;

import java.io.Serializable;

public class Product  implements Serializable {
    private String ar;
    private String arUrl;
    private String color;
    private String name;
    private String description;
    private String price;
    private String id;
    private String category;
    private String img;
    private String star;

    public Product() {
    }

    public Product(String ar, String arUrl, String color, String name, String description, String price, String category, String img, String star) {
        this.ar = ar;
        this.arUrl = arUrl;
        this.color = color;
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
        this.img = img;
        this.star = star;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAr() {
        return ar;
    }

    public String getArUrl() {
        return arUrl;
    }

    public String getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public String getCategory() {
        return category;
    }

    public String getImg() {
        return img;
    }

    public String getStar() {
        return star;
    }
}

