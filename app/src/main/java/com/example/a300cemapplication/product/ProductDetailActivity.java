package com.example.a300cemapplication.product;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a300cemapplication.R;

import com.example.a300cemapplication.shoppingcart.ShoppingItem;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ProductDetailActivity extends AppCompatActivity {
    private Product product;

    private ArrayList<ShoppingItem> productList = new ArrayList<>();

    private TextView nameTV, priceTV, categoryTV, colorTV, descriptionTV, starTV;

    private ImageView img, iv_ar;

    private Button btn;

    private FirebaseUser user;


    private DatabaseReference dbReference;

    private String userId;

    private int qty = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        nameTV = findViewById(R.id.product_name);
        priceTV = findViewById(R.id.product_price);
        categoryTV = findViewById(R.id.product_category);
        colorTV = findViewById((R.id.product_color));
        descriptionTV = findViewById(R.id.product_des);
        starTV = findViewById(R.id.product_rating);
        img = findViewById(R.id.product_img);
        btn = findViewById(R.id.btn_addCart);
        iv_ar = findViewById(R.id.imgV_ar);


        Intent intent = getIntent();
        product = (Product) intent.getSerializableExtra("Product");

        nameTV.setText(product.getName());
        priceTV.setText("$ "+product.getPrice());
        categoryTV.setText(product.getCategory());
        colorTV.setText(product.getColor());
        descriptionTV.setText(product.getDescription());
        starTV.setText("Rating("+product.getStar()+")");
        Picasso.with(this).load(product.getImg()).into(img);


        //Get user id
        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user==null){
            userId ="";
        }else{
            userId = user.getUid();
        }


        dbReference = FirebaseDatabase.getInstance().getReference().child("ShoppingCart").child(userId).child(product.getId());

        retrieveShoppingItem();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check if user logged in
                if (userId.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Sign in to get shopping", Toast.LENGTH_SHORT).show();
                } else {
                    retrieveShoppingItem();
                    addToCart();
                }

            }
        });

        if (product.getAr().equals("N")) {
            iv_ar.setVisibility(View.GONE);
        }
    }

    //user's shopping cart item
    private void retrieveShoppingItem() {
        dbReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                productList.clear();
                //put item into list
                for (DataSnapshot snap : snapshot.getChildren()) {
                    ShoppingItem currentItem = snap.getValue(ShoppingItem.class);
                    currentItem.setProductId(snap.getKey());
                    productList.add(currentItem);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    //add to firebasedatabase
    private void addToCart() {
        if (!productList.isEmpty()) {
            qty = Integer.parseInt(productList.get(0).getQuantity());
            dbReference.child(productList.get(0).getProductId()).child("quantity").setValue(Integer.toString(qty + 1));
            Toast.makeText(ProductDetailActivity.this, "Added to Cart", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            String key = dbReference.push().getKey();
            ShoppingItem item = new ShoppingItem(product.getName(), product.getPrice(), product.getCategory(), product.getColor(), Integer.toString(qty), product.getImg());
            dbReference.child(key).setValue(item);
            Toast.makeText(ProductDetailActivity.this, "Added to Cart", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    //Go to AR
    public void showAR(View view) {
        Intent intent = new Intent(getApplicationContext(), com.example.a300cemapplication.ar.ArActivity.class);
        intent.putExtra("ar_product", product);
        startActivity(intent);
    }

    //Go back to previous page
    public void back(View view) {
        finish();
    }
}