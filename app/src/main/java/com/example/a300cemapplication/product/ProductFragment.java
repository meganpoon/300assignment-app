package com.example.a300cemapplication.product;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.SearchView;


import com.example.a300cemapplication.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ProductFragment extends Fragment {

    private GridView gridView;
    private SearchView searchView;
    private ProductAdapter adapter;

    private DatabaseReference dbreference;
    private ArrayList<Product> products = new ArrayList<>();

    public ProductFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, parent, false);
        gridView = view.findViewById(R.id.grid_view);
        searchView = view.findViewById(R.id.homepage_sv);
        getProduct();
        searchProduct();
        return view;
    }

    //Get Product Data From Firebase
    private void getProduct() {
        dbreference = FirebaseDatabase.getInstance().getReference().child("Product");
        dbreference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                products.clear();
                for (DataSnapshot datas : snapshot.getChildren()) {
                    Product product = datas.getValue(Product.class);
                    product.setId(datas.getKey());
                    products.add(product);
                }

                adapter = new ProductAdapter(getContext(), products);
                gridView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void searchProduct() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
               return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return true;
            }
        });

    }



}