package com.example.a300cemapplication.product;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a300cemapplication.R;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductAdapter extends BaseAdapter implements Filterable {

    private Context ctx;
    private ArrayList<Product> filterProductList = new ArrayList<>();
    private ArrayList<Product> products = new ArrayList<>();


    public ProductAdapter(Context ctx, ArrayList<Product> filterProductList) {
        this.ctx = ctx;
        this.filterProductList = filterProductList;
        this.products = filterProductList;
    }


    @Override
    public int getCount() {
        return filterProductList.size();
    }

    @Override
    public Product getItem(int position) {
        return filterProductList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View v = inflater.inflate(R.layout.fragment_product, parent, false);

        Product product = filterProductList.get(position);
        ImageView imageView = (ImageView) v.findViewById(R.id.product_img);
        TextView priceTV =v.findViewById(R.id.product_price);
        TextView nameTV = v.findViewById(R.id.product_name);



        //Set Product Image
        Picasso.with(ctx).load(product.getImg()).into(imageView);

        //Set Product Price, Name
        priceTV.setText("$ " + product.getPrice());
        nameTV.setText(product.getName());

        //implement setOnItemClickListener event
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Send intent to ProductDetailActivity
                Intent intent = new Intent(ctx, ProductDetailActivity.class);
                // Pass product index
                intent.putExtra("Product", (Serializable) product);
                ctx.startActivity(intent);
            }
        });

        return v;
    }

    //filter implementation
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filterProductList = products;
                } else {
                    ArrayList<Product> filteredList = new ArrayList<>();
                    for (Product pd : products) {
                        //if match query string add to filteredList
                        if (pd.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(pd);
                        }
                    }
                    filterProductList = filteredList;
                }
               // return filtered Results
                FilterResults filterResults = new FilterResults();
                filterResults.values = filterProductList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterProductList = (ArrayList<Product>)filterResults.values;
                // Refresh list with filterProductList
                notifyDataSetChanged();
            }
        };
    }
                }