package com.example.a300cemapplication.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.a300cemapplication.MainActivity;
import com.example.a300cemapplication.R;
import com.example.a300cemapplication.product.Product;
import com.example.a300cemapplication.product.ProductAdapter;
import com.example.a300cemapplication.product.ProductDetailActivity;
import com.example.a300cemapplication.product.ProductFragment;

import java.util.Locale;


public class HomeFragment extends Fragment {

    private ImageView img;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home, container, false);

        img = view.findViewById(R.id.setting);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        return view;
    }


    //Language setting
    private void showDialog() {
        final AlertDialog.Builder normalDialog = new AlertDialog.Builder(getContext());
        normalDialog.setIcon(R.drawable.language);
        normalDialog.setTitle("SETTING");
        normalDialog.setMessage("Select Language");
        normalDialog.setPositiveButton("English",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        languageSetting("en");
                        startActivity(new Intent(getContext(), MainActivity.class));
                    }
                });
        normalDialog.setNegativeButton("中文",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        languageSetting("zh");
                        startActivity(new Intent(getContext(), MainActivity.class));
                    }
                });
        //show Dialog
        normalDialog.show();
    }

//set up user chosen language and use SharedPreferences to save language
    private void languageSetting(String language) {
        Locale locale = new Locale(language);
        locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getContext().getResources().updateConfiguration(configuration, getContext().getResources().getDisplayMetrics());
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("languageSetting", getContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("languageSetting", language);
        editor.apply();
    }


}