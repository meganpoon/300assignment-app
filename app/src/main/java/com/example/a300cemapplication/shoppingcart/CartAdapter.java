package com.example.a300cemapplication.shoppingcart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a300cemapplication.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.viewHolder> {

    private Context ctx;

    private ArrayList<ShoppingItem> item = new ArrayList<>();


    public CartAdapter(Context ctx, ArrayList<ShoppingItem> item) {
        this.ctx = ctx;
        this.item = item;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.cart_row, parent, false);
        viewHolder vHolder = new viewHolder(view);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        ShoppingItem currentItem = this.item.get(position);
        holder.name.setText(currentItem.getName());
        holder.price.setText("$ "+currentItem.getPrice());
        holder.qty.setText("QTY: "+currentItem.getQuantity());

        //Set Shopping cart item(Product) Image
        Picasso.with(ctx).load(currentItem.getImg()).into(holder.img);

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        TextView name, price, qty;
        ImageView img;

        public viewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.product_name);
            price = itemView.findViewById(R.id.product_price);
            qty = itemView.findViewById(R.id.product_qty);
            img = itemView.findViewById(R.id.img);

        }
    }




}
