package com.example.a300cemapplication.shoppingcart;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;


import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;

import android.widget.Toast;

import com.example.a300cemapplication.R;
import com.example.a300cemapplication.login.LoginFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;


public class CartFragment extends Fragment {
    private Paint p;
    private TextPaint textPaint;
    private String userId;
    private RecyclerView recyclerView;
    private Button btn;
    private DatabaseReference dbReference;

    private CartAdapter adapter;
    private ShoppingItem shoppingItem;

    private ArrayList<ShoppingItem> shoppingcart = new ArrayList<>();

    private FirebaseUser user;

    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cart, container, false);



        p = new Paint();
        textPaint = new TextPaint();
        textPaint.setAntiAlias(true);
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(30);

        recyclerView = view.findViewById(R.id.rv_shoppingcart);
        btn = view.findViewById(R.id.btn_checkout);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkout();

            }
        });

        user = FirebaseAuth.getInstance().getCurrentUser();

        if(user==null){
            Toast.makeText(getContext(), "Please login to continue", Toast.LENGTH_SHORT).show();
            Fragment fragment = new LoginFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.cartFragment, fragment);
            transaction.commit();
        }else{
            userId = user.getUid();
            //reference to firebasebatabase table ShoppingCart and specific user
            dbReference = FirebaseDatabase.getInstance().getReference().child("ShoppingCart").child(userId);
            retrieveCart();
        }

        return view;
    }


    private void retrieveCart() {
        //Get shopping cart items
        dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                shoppingcart.clear();
                for (DataSnapshot user : snapshot.getChildren()) {
                    for (DataSnapshot product : user.getChildren()) {
                        shoppingItem = product.getValue(ShoppingItem.class);
                        shoppingItem.setUserId(user.getKey());
                        shoppingItem.setProductId(product.getKey());
                        shoppingcart.add(shoppingItem);
                    }
                }
                //set adapter on  recyclerView
                adapter = new CartAdapter(getContext(), shoppingcart);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                //item remove(swap to left detele)
                ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
                itemTouchHelper.attachToRecyclerView(recyclerView);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

//place order
    private void checkout() {
        final AlertDialog.Builder normalDialog = new AlertDialog.Builder(getContext());
        normalDialog.setIcon(R.drawable.checkout);
        normalDialog.setTitle("CHECKOUT");
        normalDialog.setMessage("Place order?");
        normalDialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clearCart();
                        Toast.makeText(getContext(), "Order Placed", Toast.LENGTH_SHORT).show();
                    }
                });
        normalDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        //show Dialog
        normalDialog.show();
    }

    //clear user shopping cart
    private void clearCart() {
        dbReference = FirebaseDatabase.getInstance().getReference().child("ShoppingCart").child(userId);
        dbReference.removeValue();
        shoppingcart.clear();
    }


    //Item touch helper implementation on swap left to delete item
    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            int fromPosition = viewHolder.getAdapterPosition();
            int toPosition = target.getAdapterPosition();
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(shoppingcart, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(shoppingcart, i, i - 1);
                }
            }
            adapter.notifyItemMoved(fromPosition, toPosition);
            return true;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            ShoppingItem item = shoppingcart.get(position);
            dbReference = FirebaseDatabase.getInstance().getReference().child("ShoppingCart")
                    .child(userId).child(item.getUserId()).child(item.getProductId());
            dbReference.removeValue();
            adapter.notifyItemRangeChanged(position, shoppingcart.size());
            shoppingcart.remove(position);
            adapter.notifyItemRemoved(position);
            Toast.makeText(getContext(), "Item Removed", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            String text = "Delete     ";
            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                View itemView = viewHolder.itemView;
                float contentHeight = itemView.getBottom() - itemView.getTop() - itemView.getPaddingTop() - itemView.getPaddingBottom();

                p.setColor(Color.RED);
                RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                c.drawRect(background, p);

                float textWidth = textPaint.measureText(text);
                float textX = itemView.getWidth() - itemView.getPaddingRight() - textWidth;
                float textY = itemView.getTop() + itemView.getPaddingTop() + contentHeight / 2
                        + ((Math.abs(textPaint.ascent() - Math.abs(textPaint.descent()))) / 2);
                c.drawText(text, textX, textY, textPaint);

            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };

}
