package com.example.a300cemapplication.shoppingcart;

import java.io.Serializable;

public class ShoppingItem implements Serializable {
    private String name, price, category, color, quantity, img, productId, userId;

    public ShoppingItem() {
    }

    public ShoppingItem(String name, String price, String category, String color, String quantity, String img) {
        this.name = name;
        this.price = price;
        this.category = category;
        this.color = color;
        this.quantity = quantity;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getCategory() {
        return category;
    }

    public String getColor() {
        return color;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getImg() {
        return img;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
