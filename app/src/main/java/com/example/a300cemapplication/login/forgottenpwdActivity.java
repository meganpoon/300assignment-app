package com.example.a300cemapplication.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.a300cemapplication.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class forgottenpwdActivity  extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "Forgotten Password";

    private EditText edtEmail;
    private Button btn;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgottenpwd);

        edtEmail = findViewById(R.id.edt_email);
        btn = findViewById(R.id.btn_next);
        btn.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_next:
                if(validateForm())
                    resetPassword(edtEmail.getText().toString());
                break;
        }
    }

    private void resetPassword(String email) {
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "sendPasswordResetEmail:success");
                            Toast.makeText(forgottenpwdActivity.this, "Reset password email sent", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Log.d(TAG, "sendPasswordResetEmail:failed");
                            Toast.makeText(forgottenpwdActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    //Input form validation
    private boolean validateForm() {
        if (TextUtils.isEmpty(edtEmail.getText().toString())) {
            edtEmail.setError("Email Required");
            edtEmail.requestFocus();
            return false;
        } else {
            edtEmail.setError(null);
            return true;
        }
    }

    //Go back to previous page
    public void back(View view) {
        finish();
    }
}