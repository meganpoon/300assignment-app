package com.example.a300cemapplication.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.a300cemapplication.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "Login";


    //Parameter declarations
    private Button btnSignIn, btnRegister, btnForgotten;

    private EditText edtEmail, edtPassword;

    private FirebaseAuth mAuth;


    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        btnSignIn = view.findViewById(R.id.btn_signin);
        btnRegister = view.findViewById(R.id.btn_register);
        btnForgotten = view.findViewById(R.id.btn_forgotten);
        edtEmail = view.findViewById(R.id.email);
        edtPassword = view.findViewById(R.id.password);

        btnSignIn.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        btnForgotten.setOnClickListener(this);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_signin:
                if (validateForm()) {
                    signIn(edtEmail.getText().toString(), edtPassword.getText().toString());
                }
                break;
            case R.id.btn_register:

                register();
                break;
            case R.id.btn_forgotten:
                forgottenpwd();
                break;
        }
    }

    //Go to forgot password page
    private void forgottenpwd() {
        Intent intent = new Intent(getContext(), forgottenpwdActivity.class);
        getContext().startActivity(intent);
    }

    //Go to forgot register page
    private void register() {
        Intent intent = new Intent(getContext(), SignUpActivity.class);
        getContext().startActivity(intent);
    }

    private void signIn(String email, String password) {
        ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Login");
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    progressDialog.dismiss();
                    FirebaseUser user = mAuth.getCurrentUser();
                    Log.d(TAG, "signInWithEmail:success");
                    Toast.makeText(getContext(), "Authentication Successful.", Toast.LENGTH_SHORT).show();

                    // SharedPreferences - User
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", getContext().MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("UID", user.getUid().toString());
                    editor.commit();

                    //Go back to home page
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    Intent intent = new Intent(getContext(), com.example.a300cemapplication.MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);
                } else {
                    progressDialog.dismiss();
                    // If sign in fails, display a message to the user.
                    Log.d(TAG, "signInWithEmail:failure", task.getException());
                    Toast.makeText(getContext(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            }
        });
    }

    //Input form validation(not null)
    private boolean validateForm() {
        if (TextUtils.isEmpty(edtEmail.getText().toString())) {
            edtEmail.setError("Email Required");
            edtEmail.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(edtPassword.getText().toString())) {
            edtPassword.setError("Password Required");
            edtPassword.requestFocus();
            return false;
        } else {
            edtEmail.setError(null);
            edtPassword.setError(null);
            return true;
        }
    }
}