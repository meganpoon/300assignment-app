package com.example.a300cemapplication.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.a300cemapplication.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Register";

    private EditText edtEmail, edtPassword;
    private Button btn;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_sign_up);

        edtEmail = findViewById(R.id.email);
        edtPassword = findViewById(R.id.password);
        btn = findViewById(R.id.btn_register);
        btn.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                if (validateForm())
                    register(edtEmail.getText().toString(), edtPassword.getText().toString());
                break;
        }
    }

    private void register(String email, String password) {

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success
                    Log.d(TAG, "createUserWithEmail:success");
                    FirebaseUser user = mAuth.getCurrentUser();
                    //send email verification to user register email addresss
                    user.sendEmailVerification();
                    Toast.makeText(SignUpActivity.this, "Done. Please verify your email",
                            Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    // Sign in failed
                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
                    Toast.makeText(SignUpActivity.this, "Register failed.",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    //Input form validation
    private boolean validateForm() {
        if (TextUtils.isEmpty(edtEmail.getText().toString())) {
            edtEmail.setError("Email Required");
            edtEmail.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(edtPassword.getText().toString())) {
            edtPassword.setError("Password Required");
            edtPassword.requestFocus();
            return false;
        } else {
            edtEmail.setError(null);
            edtPassword.setError(null);
            return true;
        }
    }

    //Go back to previous page
    public void back(View view) {
        finish();
    }
}